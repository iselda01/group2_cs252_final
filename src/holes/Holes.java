package holes;

//import necessary packages
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.util.*;
import javax.swing.*;
import java.util.Random;
import javax.swing.border.EmptyBorder;
import javax.swing.BorderFactory;
import javax.swing.border.CompoundBorder;
import javax.sound.sampled.AudioInputStream; 
import javax.sound.sampled.AudioSystem; 
import javax.sound.sampled.Clip; 
import javax.sound.sampled.LineUnavailableException; 
import javax.sound.sampled.UnsupportedAudioFileException; 
import java.io.*;
import javax.imageio.ImageIO;

public class Holes {

    public static void main(String[] args) {

        EventQueue.invokeLater(
                new Runnable() {
            @Override
            public void run() {
                //Create the ProgramFrame object
                ProgramFrame frame = new ProgramFrame();

                frame.setTitle("Holes");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
            }
        }
        );
    }
}

class ProgramFrame extends JFrame {

    //Create variables that will be all of the different components and panels
    //that will be added/removed from the frame so they can easily accessed.
    private ProgramFrame programFrame;
    private EllipseComponent ellipseComponent;
    private EndOfGameComponent endOfGameComponent;
    private StartOfGameComponent startOfGameComponent;
    private RestoreGameComponent restoreGameComponent;
    private DisplayPanel displayPanel;
    private ButtonPanel buttonPanel;
    private HallOfFamePanel hallOfFamePanel;
    
    //Create an int to represent the level
    //Create an array of HallOfFameRecord objects that will represent
    //the ordered HallOfFame
    //Create an array of Colors that will be the different background colors
    //for the levels
    private int myLevel;
    private HallOfFameRecord [] hallOfFame;
    private static final Color [] levelColors = new Color [8];
    
    //create booleans to tell whether the application is muted,whether
    //the hall of fame is shown, and whether the game is paused.
    private boolean isMuted = false;
    private boolean isHallOfFameShown = true;
    private boolean isPaused = false;

    //Create two ints that will represent the score and lives.
    private int myScore;
    private int myLives;

    //a method to get the score, increase the score (this will be public,
    //as the score will need to be increased by methods outside of this
    //object), and to set the score to a totally different int.
    private int score() {
        return myScore;
    }

    public void increaseScore(int points) {
        myScore = score() + points;
    }

    private void setScore(int other) {
        myScore = other;
    }
    
    //methods to access, set, and increment the level
    private int level() {
        return myLevel;
    }

    public void increaseLevel() {
        myLevel++;
    }

    private void setLevel(int other) {
        myLevel = other;
    }
    
    //a method to determine how many points are needed to advance to the
    //next level
    private int pointsNeeded(){
        return level() * level() * 20;
    }
    
    //methods to get, set, and decrement the lives.
    private int lives() {
        return myLives;
    }
    
    private void setLives(int other) {
        myLives = other;
    }
    
    private void loseLife(){
        myLives--;
    }
    
    public ProgramFrame() {
        //fill in levelColors with 8 different colors
        levelColors[0] = Color.LIGHT_GRAY;
        levelColors[1] = Color.ORANGE;
        levelColors[2] = Color.YELLOW;
        levelColors[3] = Color.GREEN;
        levelColors[4] = Color.BLUE;
        levelColors[5] = Color.CYAN;
        levelColors[6] = Color.PINK;
        levelColors[7] = Color.MAGENTA;
        
        //set the layout to a BorderLayout so the components and panels can
        //be added in the correct places.
        setLayout(new BorderLayout());
        
        //set this to the programFrame so it and its methods can be accessed.
        programFrame = this;
        
        //create a StartOfGameComponent that will appear at the start of the 
        //application
        startOfGameComponent = new StartOfGameComponent("");
        
        //create the DisplayPanel that will display the score, level, and
        //remaining lives.
        displayPanel = new DisplayPanel();

        //set the border to one that will have a solid line and will have a
        //small margin around it.
        displayPanel.setBorder(new CompoundBorder(BorderFactory.createLineBorder(Color.black), displayPanel.getBorder()));

        //create the ButtonPanel that will contain the buttons with the
        //functionalities to save, restore a save, start a new game, pause a
        //game, toggle the hall of fame panel, and mute sounds
        buttonPanel = new ButtonPanel();

        //set the border to one that will have a solid line and will have a
        //small margin around it.
        buttonPanel.setBorder(new CompoundBorder(BorderFactory.createLineBorder(Color.black), buttonPanel.getBorder()));
        
        //read in the saved hallOfFame from "HallOfFame"
        try {
            FileInputStream fileIn = new FileInputStream("HallOfFame");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            hallOfFame = (HallOfFameRecord []) in.readObject();
            in.close();
            fileIn.close();
        } 
        catch (IOException i) {
            i.printStackTrace();
            return;
        }
        catch (ClassNotFoundException c) {
            System.out.println("HallOfFameRecord class not found");
            c.printStackTrace();
            return;
         }
        
        //create the HallOfFamePanel that will display the 16 highest scores.
        hallOfFamePanel = new HallOfFamePanel();
        
        //set the border to one that will have a solid line and will have a
        //small margin around it.
        hallOfFamePanel.setBorder(new CompoundBorder(BorderFactory.createLineBorder(Color.black), hallOfFamePanel.getBorder()));
        
        //add these panels and component to the frame and pack.
        add(startOfGameComponent, "Center");
        add(displayPanel, "North");
        add(buttonPanel, "South");
        add(hallOfFamePanel, "East");
        pack();
    }
    
    class HallOfFamePanel extends JPanel{
        
        //Set the width to an appropiate size
        @Override
        public Dimension getPreferredSize() {
            return new Dimension(200, 100);
        }
        
        public HallOfFamePanel() {
            //create the margin here
            setBorder(new EmptyBorder(30, 30, 30, 30));

            //set the layout so that "Hall Of Fame" can be displayed over
            //the 16 highest scores.
            setLayout(new GridLayout(17, 1));
            add(new JLabel("Hall Of Fame"));

            //add the scores
            for (int hallNumber = 0; hallNumber < 16; hallNumber++) {
                add(new JLabel("" + (hallNumber + 1) + ". " + hallOfFame[hallNumber].name + ": " + hallOfFame[hallNumber].score));
                //
                //inv:
                //  JLabels [hallNumber + 1... 16] have not been added
                //
            }
        }
    }

    class ButtonPanel extends JPanel {

        //set the panel to an appropriate size
        @Override
        public Dimension getPreferredSize() {
            return new Dimension(100, 65);
        }

        public ButtonPanel() {
            //create the margin here
            setBorder(new EmptyBorder(20, 10, 10, 10));

            //set the layout so that there is space between each button
            setLayout(new GridLayout(1, 6, 10, 0));

            //add the buttons and add the appropriate Actions to each.
            JButton muteButton = new JButton("Mute");
            muteButton.addActionListener(new muteAction());
            
            JButton pauseButton = new JButton("Pause");
            pauseButton.addActionListener(new pauseAction());
            
            JButton hallOfFameButton = new JButton("Hall of Fame");
            hallOfFameButton.addActionListener(new hallOfFameAction());
            
            JButton newGameButton = new JButton("New Game");
            newGameButton.addActionListener(new newGameAction());
            
            add(muteButton);
            add(hallOfFameButton);
            
            JButton saveButton = new JButton("Save");
            saveButton.addActionListener(new saveAction());
            
            JButton restoreButton = new JButton("Restore");
            restoreButton.addActionListener(new restoreAction());
            
            add(saveButton);
            add(restoreButton);
            add(pauseButton);
            add(newGameButton);
        }
    }
    
    class restoreAction implements ActionListener{
        public void actionPerformed(ActionEvent e){
            //remove whatever component is currently in the Center space of
            //the programFrame
            if (endOfGameComponent != null)
                programFrame.remove(endOfGameComponent);
            
            if (ellipseComponent != null)
                programFrame.remove(ellipseComponent);
            
            if (startOfGameComponent != null)
                programFrame.remove(startOfGameComponent);
            
            if (restoreGameComponent != null)
                programFrame.remove(restoreGameComponent);
            
            //add a new RestoreGameComponent to the Center of programFrame
            restoreGameComponent = new RestoreGameComponent();
            programFrame.add(restoreGameComponent);
            programFrame.revalidate();
            programFrame.repaint();
            
        }
    }
    class saveAction implements ActionListener{
        //This ArrayList will represent an ArrayList of all the saved games
        //that have been previously saved and append the current game to it
        ArrayList < GameState > savedGames;
        
        public void actionPerformed(ActionEvent e){
            //first check to see if the ellipseComponent even exists
            if (ellipseComponent != null){
                //check to make sure the game isn't over
                if (lives() > 0){
                    //then set savedGames to the object from the file "SavedGames"
                    try {
                        FileInputStream fileIn = new FileInputStream("SavedGames");
                        ObjectInputStream in = new ObjectInputStream(fileIn);
                        savedGames = (ArrayList < GameState >) in.readObject();
                        in.close();
                        fileIn.close();
                    } 
                    catch (IOException i) {
                        i.printStackTrace();
                        return;
                    }
                    catch (ClassNotFoundException c) {
                        System.out.println("GameState class not found");
                        c.printStackTrace();
                        return;
                    }


                    //add the current game
                    GameState currentGame = new GameState(score(), level(), lives(), ellipseComponent.rows(), ellipseComponent.cols(), ellipseComponent.time());
                    savedGames.add(currentGame);

                    //write savedGames back to "SavedGames"
                    try {
                        FileOutputStream fileOut = new FileOutputStream("SavedGames");
                        ObjectOutputStream out = new ObjectOutputStream(fileOut);
                        out.writeObject(savedGames);
                        out.close();
                        fileOut.close();
                    } 
                    catch (IOException i) {
                        i.printStackTrace();
                    }
                }
            }
        }
    }
    
    class newGameAction implements ActionListener{
        public void actionPerformed(ActionEvent e){
            //remove whatever component is currently in the Center of
            //programFrame
            if(ellipseComponent != null)
                programFrame.remove(ellipseComponent);
            
            if (endOfGameComponent != null) 
                programFrame.remove(endOfGameComponent);
            
            if (restoreGameComponent != null)
                programFrame.remove(restoreGameComponent);
            
            if (startOfGameComponent != null)
                programFrame.remove(startOfGameComponent);
            
            //add a new StartOfGameComponent to the center of programFrame
            startOfGameComponent = new StartOfGameComponent("");
            programFrame.add(startOfGameComponent, "Center");
            
            programFrame.revalidate();
            programFrame.repaint();
        }
    }
    
    class muteAction implements ActionListener{
        
        public void actionPerformed(ActionEvent e){
            //switch isMuted to the opposite of what it currently is
            isMuted = !isMuted;
        }
    }
    
    class pauseAction implements ActionListener{
        
        public void actionPerformed(ActionEvent e){
            //switch isPaused to the opposite of what it currently is
            isPaused = !isPaused;
            
            //restart the timer when unpaused using the repaint method
            if(!isPaused){
                repaint();
            }
            else{
                ellipseComponent.timer().cancel();
                repaint();
            }
        }
    }
    
    class hallOfFameAction implements ActionListener{
        
        public void actionPerformed(ActionEvent e){
            //if the HOF is shown, remove it from programFrame and repaint it
            //also flip isHallOfFameShown
            if (isHallOfFameShown){
                programFrame.remove(hallOfFamePanel);
                programFrame.repaint();
                isHallOfFameShown = !isHallOfFameShown;
            }
            //if the HOF is not shown, add a new one to the East of programFrame
            //also flip isHallOfFameShown
            else{
                hallOfFamePanel = new HallOfFamePanel();
                hallOfFamePanel.setBorder(new CompoundBorder(BorderFactory.createLineBorder(Color.black), hallOfFamePanel.getBorder()));
                add(hallOfFamePanel, "East");
                hallOfFamePanel.revalidate();
                hallOfFamePanel.repaint();
                isHallOfFameShown = !isHallOfFameShown;
            }
        }
    }

    class DisplayPanel extends JPanel {

        //create variables for the three different labels to be displayed
        private JLabel myScoreLabel = new JLabel();
        private JLabel myLevelLabel = new JLabel();
        private JLabel myLivesLabel = new JLabel();

        //getters and setters for these labels
        private JLabel scoreLabel() {
            return myScoreLabel;
        }

        private void setScoreLabel(String other) {
            myScoreLabel.setText(other);
        }

        private JLabel levelLabel() {
            return myLevelLabel;
        }

        private void setLevelLabel(String other) {
            myLevelLabel.setText(other);
        }

        private JLabel livesLabel() {
            return myLivesLabel;
        }

        private void setLivesLabel(String other) {
            myLivesLabel.setText(other);
        }

        //set the panel to an appropriate size
        @Override
        public Dimension getPreferredSize() {
            return new Dimension(100, 65);
        }

        //override the paintComponent method so that the score can be updated
        //whenever the panel is repainted and the labels can be resized.
        @Override
        public void paintComponent(Graphics canvas) {
            Font displayFont = new Font("SansSerif", 0, (int) (getSize().getWidth() * .06));
            scoreLabel().setFont(displayFont);
            levelLabel().setFont(displayFont);
            livesLabel().setFont(displayFont);

            setScoreLabel("Score: " + score());
            setLevelLabel("Level: " + level());
            setLivesLabel("Lives: " + lives());

            super.paintComponent(canvas);
        }

        public DisplayPanel() {
            //set the layout so the labels can be well spaced
            setLayout(new GridLayout(1, 3));

            //set the initial text for the labels
            setScoreLabel("Score: " + score());
            setLevelLabel("Level: " + level());
            setLivesLabel("Lives: " + lives());

            //make the labels visible
            scoreLabel().setOpaque(true);
            levelLabel().setOpaque(true);
            livesLabel().setOpaque(true);

            //add the labels to the panel
            add(scoreLabel());
            add(levelLabel());
            add(livesLabel());
            repaint();
        }
    }

    class EllipseComponent extends JComponent {

        //These int's will represent what size the ellipses should be when the
        //component is initially painted
        private static final int OUR_DEFAULT_ELLIPSE_WIDTH = 100;
        private static final int OUR_DEFAULT_ELLIPSE_HEIGHT = 100;

        //myEllipses will contain all of the ellipses that are currently displayed.
        //The ellipses will be ordered as they appear on the screen, with the first
        //row being filled first, followed by the second row, etc.
        //myCurrentEllipse will represent the index within myEllipses of a specific
        //ellipse that will be worked with
        private ArrayList< Ellipse2D> myEllipses;
        private int myCurrentEllipse;

        //myTotalNumberOfEllipses will be how many ellipses will be displayed on
        //the screen at any time
        private int myTotalNumberOfEllipses;

        //myCurrentRedEllipse will be the index within myEllipses of the ellipse
        //that is currently red.
        private int myCurrentRedEllipse = -1;

        //totalRows will be the amount of rows, and totalCols will be the amount
        //of columns.
        private int totalRows;
        private int totalCols;
        
        //myTimer will be the current and only Timer
        //myTImerTask will be the TimerTask that myTimer will use
        //myTime is the initial time before the red ellipse switches
        private java.util.Timer myTimer;
        private TimerTask myTimerTask;
        private int myTime;
        
        //creates empty BufferedImage objects
        private BufferedImage gopherBuffered;
        private BufferedImage pause;
        
        //getters and setters for myTime, myTimer, and myTimerTask
        private java.util.Timer timer(){
            return myTimer;
        }
        
        private void setTimer(java.util.Timer other){
            myTimer = other;
        }
        
        private TimerTask timerTask(){
            return myTimerTask;
        }
        
        private void setTimerTask(TimerTask other){
            myTimerTask = other;
        }
        
        private int time() {
            return myTime;
        }

        private void setTime(int other) {
            myTime = other;
        }
        
        //a method to get and set totalRows and totalCols
        private int rows() {
            return totalRows;
        }

        private void setRows(int other) {
            totalRows = other;
        }

        private int cols() {
            return totalCols;
        }

        private void setCols(int other) {
            totalCols = other;
        }

        //a method to set and get myEllipses and myCurrentEllipse
        private ArrayList< Ellipse2D> ellipses() {
            return myEllipses;
        }

        private int currentEllipse() {
            return myCurrentEllipse;
        }

        private void setEllipses(ArrayList< Ellipse2D> other) {
            myEllipses = other;
        }

        private void setCurrentEllipse(int other) {
            myCurrentEllipse = other;
        }

        //a method to get and set myTotalNumberOfEllipses
        private int totalNumberOfEllipses() {
            return myTotalNumberOfEllipses;
        }

        private void setTotalNumberOfEllipses(int other) {
            myTotalNumberOfEllipses = other;
        }

        //methods to set and get myCurrentRedEllipse
        private int currentRedEllipse() {
            return myCurrentRedEllipse;
        }

        private void setCurrentRedEllipse(int other) {
            myCurrentRedEllipse = other;
        }

        //
        //Method chooseRedEllipse
        //        
        //Purpose:
        //      To pseudo-randomly pick another ellipse index to be red on repaint
        //
        //Formals:
        //      randomEllipseNumber - the randomly selected integer that represents
        //      the index of the new red ellipse
        //          
        private void chooseRedEllipse() {
            //create a Random object, and create a pseudo-random int between 0
            //and the total number of ellipses
            Random generator = new Random();
            int randomEllipseNumber = generator.nextInt(totalNumberOfEllipses());

            //set that pseudo-random number to the current red ellipse
            setCurrentRedEllipse(randomEllipseNumber);
        }

        //The initial size will be as wide as the initial ellipse width times the
        //amount of columns and will be as tall as the initial ellipse height times
        //the amount of rows.
        @Override
        public Dimension getPreferredSize() {
            return new Dimension(OUR_DEFAULT_ELLIPSE_WIDTH * cols(), OUR_DEFAULT_ELLIPSE_HEIGHT * rows());
        }

        @Override
        public void paintComponent(Graphics canvas) {

            Graphics2D canvas2D = (Graphics2D) canvas;
            
            //paint the background to a color corresponding to the level
            canvas2D.setColor(levelColors[level() % 8]);
            
            canvas2D.fillRect(0, 0, getWidth(), getHeight());
            //choose a new red ellipse so that the red ellipse will change on
            //a window resize or when the red ellipse has been clicked
            if (!isPaused)
                chooseRedEllipse();

            //Reset all Ellipse2D objects in myEllipses so that they are the
            //correct size for the window and that they are colored correctly.
            //Also redraw those new ellipses
            for (int numberOfEllipse = 0; numberOfEllipse < ellipses().size(); numberOfEllipse++) {
                ellipses().set(numberOfEllipse, new Ellipse2D.Double((getSize().getWidth() / cols()) * (numberOfEllipse % (totalNumberOfEllipses() / rows())), (getSize().getHeight() / rows()) * (numberOfEllipse / cols()), getSize().getWidth() / cols(), getSize().getHeight() / rows()));
                if (numberOfEllipse == currentRedEllipse()) {
                    TexturePaint gopherTexture = new TexturePaint(gopherBuffered, new Rectangle2D.Double(ellipses().get(numberOfEllipse).getMinX(), ellipses().get(numberOfEllipse).getMinY(), (ellipses().get(numberOfEllipse).getMaxX()-ellipses().get(numberOfEllipse).getMinX()), (ellipses().get(numberOfEllipse).getMaxY())- ellipses().get(numberOfEllipse).getMinY()));
                    canvas2D.setPaint(gopherTexture);
                } else {
                    canvas2D.setColor(Color.BLACK);
                }

                canvas2D.fill(ellipses().get(numberOfEllipse));
                //
                //inv:
                //      no ellipse in ellipses[0 ... numberOfEllipse - 1] is filled
                //   
            }
            
            //Overlay the pause graphic if the game is paused. This has to be
            //here so that it is drawn over the ellipses
            if(isPaused){
                canvas2D.drawImage(pause.getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH), 0, 0, this);
            }
            
            //cancel the myTimer's task so that on certain times when repaint()
            //is called (such as resize), new Timer objects aren't created and
            //their tasks are not executed.
            timer().cancel();
            
            //Create a TimerTask object that will just call the repaint method
            //which will select a new red ellipse and reset myTimer to a new
            //one.
            TimerTask task = new TimerTask() {
            public void run() {
                repaint();
                }
            };
            
            //set myTimerTask to this task, and set myTimer to a timer that
            //will execute myTimerTask after 1000 milliseconds.
            
            setTimerTask(task);
            //set myTimer to a new Timer with myTimerTask as its task so that
            //the red ellipse will be changed and a new myTimer will be set.
            setTimer(new java.util.Timer("Timer"));
            timer().schedule(timerTask(), (long) (time() / level()));
            
            //stop the timer if the game is paused
            if(isPaused){
                timer().cancel();
            }
            
            super.paintComponent(canvas);
        }

        //
        //Method findEllipseContainingPoint
        //
        //Purpose:
        //      To find the index of the ellipse that contains the event's click
        //      point (if any ellipse does).
        //
        //Formals:
        //      clickPoint (in) - the point2D object that is the point at which
        //      the event clicked
        //      ellipseNumber (out) - the index of the ellipse that contains the
        //      the click point. (its value is -1 when there is no ellipse that
        //      contains the click point
        //
        //Returns:
        //      The index of the ellipse that contains the click point.
        //      -1 if no ellipse contains the click point.
        //
        public int findEllipseContainingPoint(Point2D clickPoint) {
            //this code is adapted from findSquareContainingPoint from
            //BetterMouseTest.java
            //
            //create this variable outside of the for loop so that it will still
            //be accessible after the for loop terminates
            int ellipseNumber;

            //check to see if an ellipse in myEllipses contains clickPoint. If
            //it does, terminate the loop
            for (ellipseNumber = 0;
                    ellipseNumber < ellipses().size()
                    && !ellipses().get(ellipseNumber).contains(clickPoint);
                    ++ellipseNumber)
                ;
            //
            //inv:
            //      No ellipse in ellipses()[0 ... ellipseNumber - 1] spatially
            //      contain clickPoint
            //

            //Check to see if the loop did or did not find an ellipse containing
            //clickPoint. If it did, return the index of that ellipse within
            //myEllipses. If it did not, return a value that will indicate
            //that no ellipse contains this clickPoint.
            if (ellipseNumber >= ellipses().size()) {
                return -1;
            } else {
                return ellipseNumber;
            }
        }

        //ctor for EllipseComponent given a specified amount of rows and columns
        public EllipseComponent(int rows, int cols, int lives, int time) {

            //Set myCols, myRows, and myTotalNumberOfEllpses to whatever they will
            //be for the remainder of the game, and initialize myScore to 0.
            //Set myCurrentEllipse to a value that will indicate that there is no
            //current ellipse selected
            //Set myTime and myLives to whatever was passed in.
            //set myLevel to 1.
            setTime(time);
            setLives(lives);
            setLevel(1);
            setCols(cols);
            setRows(rows);
            setTotalNumberOfEllipses(cols * rows);
            setScore(0);
            setCurrentEllipse(-1);
            isPaused = false;
            
            try {
                gopherBuffered = ImageIO.read(new File("Gopher.png"));
            } catch (IOException ex) {
                System.out.println("Unable to retrieve image");
            }
            
            try{
                pause = ImageIO.read(new File("Pause.png"));
            } catch (IOException ex){
                System.out.println("Unable to retrieve image");
            }
            

            //fill myEllipses with the specified number of ellipses of their
            //initial size
            setEllipses(new ArrayList< Ellipse2D>());
            for (int numberOfRows = 0; numberOfRows < rows; numberOfRows++) {
                for (int numberOfCols = 0; numberOfCols < cols; numberOfCols++) {
                    ellipses().add(new Ellipse2D.Double(numberOfCols * OUR_DEFAULT_ELLIPSE_WIDTH, numberOfRows * OUR_DEFAULT_ELLIPSE_HEIGHT, OUR_DEFAULT_ELLIPSE_WIDTH, OUR_DEFAULT_ELLIPSE_HEIGHT));
                }
                //
                //inv:
                //      Ellipses from [0 ... numberOfCols - 1] have been added
                //      to ellipses()
                //
                //
                //inv:
                //      Rows from [0 ... numberOfRows - 1] have been added to ellipses
                //
            }
            
            //Create a TimerTask object that will just call the repaint method
            //which will select a new red ellipse and reset myTimer to a new
            //one.
            TimerTask task = new TimerTask() {
            public void run() {
                repaint();
                }
            };
            
            //set myTimerTask to this task, and set myTimer to a timer that
            //will execute myTimerTask after 1000 milliseconds.
            setTimerTask(task);
            setTimer(new java.util.Timer("Timer"));
            timer().schedule(timerTask(), (long) (time()));
            
            //repaint the component so that these ellipses show up right when
            //the component is created
            repaint();
            
            //add a MouseHandler to handle clicks on the component
            addMouseListener(new MouseHandler());
        }
        
        //This method plays a file that is passed into it
        public void playFile(File file) throws UnsupportedAudioFileException, IOException, LineUnavailableException{
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start(); 
        }

        private class MouseHandler extends MouseAdapter {
            //Override mouseClicked so that the handler can deal with mouse clicks
            @Override
            public void mouseClicked(MouseEvent event) {
                //Stop the click actions if the game is paused
                if(!isPaused){
                    //Get the index of the ellipse at the point of click. If there is
                    //no ellipse at that point, then findEllipseContainingPoint will
                    //return -1. Set myCurrentEllipse to that index.
                    int componentAtPoint = findEllipseContainingPoint(event.getPoint());
                    setCurrentEllipse(componentAtPoint);

                    //Check to see if there is an ellipse at that point
                    //(currentEllispe() != -1), and whether the clicked ellipse is
                    //the current red ellipse. If it is, increase the score by 5 and
                    //repaint the component and displayPanel so that the score is
                    //correctly displayed and a new red ellipse is selected. If not,
                    //do nothing.
                    if (currentEllipse() != -1 && currentEllipse() == currentRedEllipse()) {

                        increaseScore(4 + level());
                        if (score() >= pointsNeeded()){
                            increaseLevel();
                            if (!isMuted){
                                //if there was a successful click play the success sound.
                                try {
                                    playFile(new File ("success.wav").getAbsoluteFile());
                                } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
                                    System.out.println("Error with playing sound.");
                                }
                            }
                        }

                        else{
                            if (!isMuted){
                                //if there was a successful click play the success sound.
                                try {
                                    playFile(new File ("correct.wav").getAbsoluteFile());
                                } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
                                    System.out.println("Error with playing sound.");
                                }
                            }
                        }

                        repaint();
                        displayPanel.repaint();
                    }
                    //if there was an unsuccessful click, play the fail sound.
                    else {
                        if (!isMuted){
                            try {
                                playFile(new File ("fail.wav").getAbsoluteFile());
                            } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
                                System.out.println("Error with playing sound.");
                            }
                        }
                        loseLife();
                        displayPanel.repaint();
                        if (lives() <= 0){
                            programFrame.remove(ellipseComponent);
                            endOfGameComponent = new EndOfGameComponent();
                            programFrame.add(endOfGameComponent, "Center");
                            programFrame.revalidate();
                            programFrame.repaint();
                        }
                    }
                }
            }
        }
    }
    
    public class StartOfGameComponent extends JComponent{
        //Fields for the JTextFields that will be added to the component
        JTextField livesField;
        JTextField timeField;
        JTextField rowField;
        JTextField colField;
        //fields for the game's lives, time, rows, and cols that were entered
        int gameLives;
        int gameTime;
        int gameRows;
        int gameCols;
        
        @Override
        public Dimension getPreferredSize() {
            return new Dimension(800, 400);
        }
        
        public StartOfGameComponent(String startStatement) {
            //set the layout to a GridLayout with 2 cols and 2 rows.
            setLayout(new GridLayout(2, 2));
            //set the JTextFields to an appropriate size
            livesField = new JTextField(5);
            timeField = new JTextField(8);
            rowField = new JTextField(5);
            colField = new JTextField(5);
            
            //create a panel that will hold the livesField
            JPanel upperLeft = new JPanel();
            upperLeft.add(new JLabel("Amount of Lives: "));
            upperLeft.add(livesField);
            
            //create a panel that will hold the timeField
            JPanel upperRight = new JPanel();
            upperRight.add(new JLabel("milliseconds before holes switch initially: "));
            upperRight.add(timeField);
            
            //create a panel that will hold the rowField and colField
            JPanel lowerLeft = new JPanel();
            lowerLeft.setLayout(new GridLayout(1, 2));
            JPanel rowPanel = new JPanel();
            JPanel colPanel = new JPanel();
            lowerLeft.add(rowPanel);
            lowerLeft.add(colPanel);
            rowPanel.add(new JLabel("Rows: "));
            rowPanel.add(rowField);
            colPanel.add(new JLabel("Columns: "));
            colPanel.add(colField);
            
            //create a panel that will hold the startStatement and the
            //submit button
            JPanel lowerRight = new JPanel();
            JLabel startLabel = new JLabel(startStatement);
            startLabel.setForeground(Color.RED);
            lowerRight.add(startLabel);
            
            JButton submitButton = new JButton("Submit");
            lowerRight.add(submitButton);
            submitButton.addActionListener(new startGameAction());
            
            //add these panels in the correct order
            add(upperLeft);
            add(upperRight);
            add(lowerLeft);
            add(lowerRight);
        }
        
        public class startGameAction implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e){
                //remove the startOfGameComponent from the center of
                //programFrame
                programFrame.remove(startOfGameComponent);
                //attempt to get an int from each field
                try {
                    gameLives = Integer.parseInt(livesField.getText());
                    gameTime = Integer.parseInt(timeField.getText());
                    gameRows = Integer.parseInt(rowField.getText());
                    gameCols = Integer.parseInt(colField.getText());
                        }
                //if this exception is caught, that means that the user
                //entered something that wasn't an integer
                catch (NumberFormatException ex) {
                    //reset the StartOfGameComponent with a message telling
                    //the user what they did wrong.
                    StartOfGameComponent newGame = new StartOfGameComponent("Please enter a positive INTEGER into ALL fields");
                    startOfGameComponent = newGame;
                    programFrame.add(newGame);
                    programFrame.revalidate();
                    programFrame.repaint();
                }
                //make sure those integers are positive, or else reset the 
                //StartOfGameComponent with an approprate message
                if (gameLives > 0 && gameTime > 0 && gameRows > 0 && gameCols > 0) {
                    //set the center of programFrame to a new EllipseComponent
                    //with those specifications
                    ellipseComponent = new EllipseComponent(gameRows, gameCols, gameLives, gameTime);
                    ellipseComponent.setBorder(new CompoundBorder(BorderFactory.createLineBorder(Color.black), ellipseComponent.getBorder()));
                    programFrame.add(ellipseComponent, "Center");
                    programFrame.revalidate();
                    programFrame.repaint();
                } 
                else {
                    StartOfGameComponent newGame = new StartOfGameComponent("Please enter a POSITIVE integer into ALL fields");
                    startOfGameComponent = newGame;
                    programFrame.add(newGame);
                    programFrame.revalidate();
                    programFrame.repaint();
                }
            }
        }
    }
    
    public class EndOfGameComponent extends JComponent{
        //JTextField that will hold the player's name
        JTextField nameField;
        
        @Override
        public Dimension getPreferredSize() {
            return new Dimension(200, 200);
        }
        
        public EndOfGameComponent() {
            //set endOfGameComponent to this
            endOfGameComponent = this;
            
            //create an approprately sized JTextField for nameField
            nameField = new JTextField(20);
            
            //set the layout to a BorderLayout
            setLayout(new BorderLayout());
            
            //create a panel and add a label and nameField to it
            JPanel centerPanel = new JPanel();
            centerPanel.add(new JLabel("Name: "));
            centerPanel.add(nameField);
            
            //create a button that will submit when clicked
            JButton submitButton = new JButton("Submit");
            submitButton.addActionListener(new endGameAction());
            
            //add the button to the panel and add the panel to the component
            centerPanel.add(submitButton);
            add(centerPanel, "Center");
            
        }
        
        public class endGameAction implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e){
                //run endGame on click and remove the endOfGameComponent
                //from the center of programFrame
                endGame();
                programFrame.remove(endOfGameComponent);
            }
        }
        
        public void endGame(){
            //record the score and submitted name into a HallOfFameRecord object
            HallOfFameRecord finishedGame = new HallOfFameRecord(score(), nameField.getText());
            //check to see if the score was better than the lowest score
            //on the HOF, if it is keep checking to see if it should be
            //higher on the list. End if the finished game's score is the 
            //highest, or if it is correctly placed
            int gameIndex = 15;
            if (finishedGame.score > hallOfFame[15].score){
                hallOfFame[15] = finishedGame;
                while (hallOfFame[0] != finishedGame && finishedGame.score > hallOfFame[gameIndex - 1].score){
                    HallOfFameRecord lesserScore = hallOfFame[gameIndex - 1];
                    hallOfFame[gameIndex - 1] = finishedGame;
                    hallOfFame[gameIndex] = lesserScore;
                    gameIndex--;
                    
                    //
                    //inv:
                    //  finishedGame is placed at hallOfFame[gameIndex]
                    //
                }
                
            //reset the hallOfFamePanel to display the newly updated HOF
            programFrame.remove(hallOfFamePanel);
            hallOfFamePanel = new HallOfFamePanel();
            hallOfFamePanel.setBorder(new CompoundBorder(BorderFactory.createLineBorder(Color.black), hallOfFamePanel.getBorder()));
            programFrame.add(hallOfFamePanel, "East");
            programFrame.revalidate();
            programFrame.repaint();
            
            }
            
            //write hallOfFame to "HallOfFame"
            try {
                FileOutputStream fileOut = new FileOutputStream("HallOfFame");
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(hallOfFame);
                out.close();
                fileOut.close();
            } 
            catch (IOException i) {
                i.printStackTrace();
            }
        }
    }
    
    public class RestoreGameComponent extends JComponent{
        //savedGames is the ArrayList of GameState objects where each GameState
        //object is a saved game
        ArrayList <GameState> savedGames;
        //gameButtons will be the JRadioButtons that will represent the
        //saved games on the screen and will allow the user to select one of
        //those saved games.
        ButtonGroup gameButtons;
        
        public RestoreGameComponent() {
            //set restoreGameComponent to this
            restoreGameComponent = this;
            
            //set savedGames to the object in "SavedGames" if possible.
            try {
                FileInputStream fileIn = new FileInputStream("SavedGames");
                ObjectInputStream in = new ObjectInputStream(fileIn);
                savedGames = (ArrayList < GameState >) in.readObject();
                in.close();
                fileIn.close();
            } 
            catch (IOException i) {
                i.printStackTrace();
                return;
            }
            catch (ClassNotFoundException c) {
                System.out.println("GameState class not found");
                c.printStackTrace();
                return;
            }
            
            //set the layout to a GridLayout of an appropriate size
            setLayout(new GridLayout(savedGames.size() + 2, 1));
            
            //set the header of the restoreGameComponent
            add(new JLabel("Choose a game to be restored"));
            //set gameButtons to a new ButtonGroup
            gameButtons = new ButtonGroup();
            
            //add a JRadioButton to gameButtons for each saved game their is
            //set the ActionCommand of that button to the index within
            //savedGames that this GameState object resides
            //add that button to the component.
            for (GameState game : savedGames){
                JRadioButton savedGameButton = new JRadioButton("" + savedGames.indexOf(game) + ". " + game.score + ", " + game.level + ", " + game.lives + ", " + game.rows + ", " + game.cols);
                savedGameButton.setActionCommand("" + savedGames.indexOf(game));
                gameButtons.add(savedGameButton);
                add(savedGameButton);
            }
            
            //add a submit button
            JButton submitButton = new JButton("Submit");
            submitButton.addActionListener(new restoreGameAction());
            add(submitButton);
            }
        
        class restoreGameAction implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e){
                //check to see if a selection was made
                if (gameButtons.getSelection() != null){
                    //get the savedGames index of the selected JRadioButton
                    int indexOfSelected = Integer.parseInt(gameButtons.getSelection().getActionCommand());
                    //set a GameState object to the selected GameState object
                    //in saved Games
                    GameState selectedGame = savedGames.get(indexOfSelected);
                    
                    //remove restoreGameComponent from the center of
                    //programFrame
                    programFrame.remove(restoreGameComponent);
                    
                    //add a new EllipseComponent to the center of programFrame
                    //and set the score and level to the correct values.
                    ellipseComponent = new EllipseComponent(selectedGame.rows, selectedGame.cols, selectedGame.lives, selectedGame.time);
                    programFrame.setScore(selectedGame.score);
                    programFrame.setLevel(selectedGame.level);


                    programFrame.add(ellipseComponent);
                    programFrame.revalidate();
                    programFrame.repaint();
                }
            }
        }
    }
}

class HallOfFameRecord implements java.io.Serializable{
        //fields for the score and the name of any particular game
        public int score;
        public String name;
        
        public HallOfFameRecord(int gameScore, String playerName){
            //set the inputed values to the correct fields
            score = gameScore;
            name = playerName;
        }
    }

class GameState implements java.io.Serializable{
        //fields to store all the necessary info about a game in an object
        int score;
        int level;
        int lives;
        int rows;
        int cols;
        int time;
        
        public GameState(int gameScore, int gameLevel, int gameLives, int gameRows, int gameCols, int gameTime){
            //set the inputted values to the correct fields
            score = gameScore;
            level = gameLevel;
            lives = gameLives;
            rows = gameRows;
            cols = gameCols;
            time = gameTime;
        }
    }



